
// RENDER functions
var viewSetUserName = function ( username ) {
  $("#idusernamespan").html(username);
}

var viewSetContent = function (content) {
  $("#idtextencrypted").val(content);
}

var viewConfirmSave = function () {
  viewAuxSetMessage("SAVE SUCCESSFULL!");
}

var viewWarningFilename = function () {
  viewAuxSetMessage("Filename must start with 'textvault'");
}

var viewWarningPassword = function () {
  viewAuxSetMessage("Password must not be empty");
}

var viewWarningNocontent = function () {
  viewAuxSetMessage("The text must not be empty");
}

var viewModeUserLoggedIn = function () {
  $("#idactiveuserdiv").css("display", "block");
  $("#idauthorizediv").css("display", "none");
}

var viewModeUserLoggedOut = function () {
  $("#idactiveuserdiv").css("display", "none");
  $("#idauthorizediv").css("display", "block");
}

var viewAuxSetMessage = function (msg) {
  $("#idmessagelayercontentspan").html(msg);
  $("#idmessagelayerdiv").css("display", "block");
  setTimeout( '$("#idmessagelayerdiv").fadeOut("3000");' , 3000 );
}

var viewClearFields = function () {
  $("#idtextencrypted").val("");
  $("#idpassword").val("");
}
