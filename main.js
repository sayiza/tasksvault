// Your Client ID can be retrieved from your project in the Google
// Developer Console, https://console.developers.google.com
var CLIENT_ID = '415087835754-ef90smsdelvnjosn8bgnsgkfert3ta1s.apps.googleusercontent.com';

var SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly',
              'https://www.googleapis.com/auth/drive.file'];

var FILENAME = 'textvaultcontent.txt';
var PASSWORD;
var LISTOFALLFILES;
var FILEIDGLOBAL;
var FILECONTENT;
var DECRYPTEDCONTENT;

var decrypt = function (password , content) {
  try {
    return sjcl.decrypt(password,content);
  } catch( e ) { return ""; }
}

var encrypt = function (password , content) {
  try {
    return sjcl.encrypt(password,content);
  } catch( e ) { return ""; }
}

var reloadFiles = function (newName,event) {
  if (!event.keyCode || event.keyCode === 13 ) {
    FILENAME = newName;
    FILEIDGLOBAL = null;
    console.log("filename changed to " + FILENAME,event);    
    getAllFiles();
    viewClearFields();
  }
}

/**
 * Check if current user has authorized this application.
 */
var checkAuth = function() {
  gapi.auth.authorize(
    {
      'client_id': CLIENT_ID,
      'scope': SCOPES.join(' '),
      'immediate': true
    }, handleAuthResult);
}

/**
 * Handle response from authorization server.
 *
 * @param {Object} authResult Authorization result.
 */
var handleAuthResult = function (authResult) {
  //var authorizeDiv = document.getElementById('authorize-div');
  if (authResult && !authResult.error) {
    // Hide auth UI, then load client library.
    //authorizeDiv.style.display = 'none';
    loadDriveApi();
    viewModeUserLoggedIn();
  } else {
    viewModeUserLoggedOut();
    // Show auth UI, allowing the user to initiate authorization by
    // clicking authorize button.
    //authorizeDiv.style.display = 'inline';
  }
}

/**
 * Initiate auth flow in response to user clicking authorize button.
 *
 * @param {Event} event Button click event.
 */
var handleAuthClick = function (event) {
  gapi.auth.authorize(
    {client_id: CLIENT_ID, scope: SCOPES, immediate: false},
    handleAuthResult);
  return false;
}

/**
 * Load Drive API client library.
 */
var loadDriveApi = function() {
  gapi.client.load('drive', 'v3', getAllFiles);
}
var saveContent = function(data) {
  if ( !!!FILENAME.match(/^textvault/) ) {
    viewWarningFilename();
    return;
  }
  if ( !!!PASSWORD ) {
    viewWarningPassword();
    return;
  }
  if ( !!!data ) {
    viewWarningNocontent();
    return;
  }

  var encdata = encrypt(PASSWORD,data);
  if (!!FILEIDGLOBAL) {
    saveForUpdate( encdata, FILEIDGLOBAL, viewConfirmSave )
  } else {
    saveNewToDrive( encdata , viewConfirmSave )
  }
}

var saveForUpdate = function(data, fileId, callback) {
  const boundary = '-------314159265358979323846';
  const delimiter = "\r\n--" + boundary + "\r\n";
  const close_delim = "\r\n--" + boundary + "--";

  const contentType = 'application/json';

  var metadata = {
      'name': FILENAME,
      'mimeType': contentType
    };

    var multipartRequestBody =
        delimiter +
        'Content-Type: application/json\r\n\r\n' +
        JSON.stringify(metadata) +
        delimiter +
        'Content-Type: ' + contentType + '\r\n\r\n' +
        data +
        close_delim;

    var request = gapi.client.request({
        'path': '/upload/drive/v3/files/'+fileId,
        'method': 'PATCH',
        'params': {'uploadType': 'multipart'},
        'headers': {
          'Content-Type': 'multipart/related; boundary="' + boundary + '"'
        },
        'body': multipartRequestBody});
    if (!callback) {
      callback = function(file) {
        console.log(file)
      };
    }
    request.execute(callback);
}

var saveNewToDrive = function(data,callback) {
  const boundary = '-------314159265358979323846';
  const delimiter = "\r\n--" + boundary + "\r\n";
  const close_delim = "\r\n--" + boundary + "--";

  const contentType = 'application/json';

  var metadata = {
      'name': FILENAME,
      'mimeType': contentType
    };

    var multipartRequestBody =
        delimiter +
        'Content-Type: application/json\r\n\r\n' +
        JSON.stringify(metadata) +
        delimiter +
        'Content-Type: ' + contentType + '\r\n\r\n' +
        data +
        close_delim;

    var request = gapi.client.request({
        'path': '/upload/drive/v3/files',
        'method': 'POST',
        'params': {'uploadType': 'multipart'},
        'headers': {
          'Content-Type': 'multipart/related; boundary="' + boundary + '"'
        },
        'body': multipartRequestBody});
    if (!callback) {
      callback = function(file) {
        console.log(file)
      };
    }
    request.execute(callback);
}

var updatePassword = function(newpw) {
  PASSWORD = newpw;
  viewSetContent( decrypt( PASSWORD , FILECONTENT ));
}

var getTheOneFileContent = function(fileId) {
    var request = new XMLHttpRequest();
    var path="https://www.googleapis.com/drive/v3/files/"+fileId+"?alt=media";

    var state_change = function() {
      if (request.readyState==4) { // 4 = "loaded"
        if (request.status==200){// 200 = OK
          FILECONTENT = request.response;
          FILEIDGLOBAL = fileId;
          console.log('ok it is', request.response );
          viewSetContent( decrypt( PASSWORD , FILECONTENT ));
        } else {
          console.log("Problem retrieving XML data");
        }
      }
    }

    request.onreadystatechange = state_change;

    request.open("GET", path, true);
    //request.setRequestHeader("Referer", "http://www.google.com");
    //request.setRequestHeader("User-Agent", "Mozilla/5.0");
    request.setRequestHeader("Accept","text/plain");
    request.setRequestHeader('Authorization', 'Bearer '+ gapi.auth.getToken().access_token);
    //request.setRequestHeader("Content-Type","text/plain");

    request.send(null);
}

//var handleFILECONTENT = function (json) {
//  console.log("this is the jsonp callback fnc");
//  console.log(json);
//}
/**
 * Get all file-names.
 */
var getAllFiles = function() {
    var requestFiles = gapi.client.drive.files.list({
      'pageSize': 1000,
      'fields': "nextPageToken, files(id, name)" //webContentLink
    });

    requestFiles.execute(function(resp) {
      //appendPre('Files:');
      //var files = resp.files;
      var fileExists = false;
      var fileId     = null;
      LISTOFALLFILES = resp.files;
      if (LISTOFALLFILES && LISTOFALLFILES.length > 0) {
        for (var i = 0; i < LISTOFALLFILES.length; i++) {
          if (LISTOFALLFILES[i].name === FILENAME && !!FILENAME.match(/^textvault/) ) {
            fileExists = true;
            fileId     = LISTOFALLFILES[i].id;
            console.log("found the file:",LISTOFALLFILES[i]);

            //$.ajax({
            //    url: LISTOFALLFILES[i].webContentLink
            //  , dataType: "jsonp"
              //, jsonp: "callback"
              //, jsonpCallback: "handleFILECONTENT"
            //  , success: function (data) { console.log("this is the content", data) }
            //});
          }
          //var file = files[i];
          //appendPre(file.name + ' (' + file.id + ')');
        }
      }
      console.log(LISTOFALLFILES);

      if (!fileExists) {
        console.log("file not found on drive");
      } else {
        getTheOneFileContent(fileId);
        //var requestTheFile = gapi.client.drive.files.get({
        //  'fileId': fileId,
        //  'alt': 'media'
        //});
        //requestTheFile.execute(function(resp) {
        //  console.log("this is the 1 file", resp);
        //});

        //var requestTheFile = gapi.client.request({
        //    'path': '/drive/v3/files/'+fileId,
        //    'method': 'GET',
        //    'params': {'alt': 'media'},
        //    });
        //requestTheFile.execute(function(resp) {console.log(resp)});

        //$.ajax({
        //  url: "https://www.googleapis.com/drive/v3/files/"+fileId+"?alt=media",
          //data: { signature: authHeader },
          //type: "GET",
        //  beforeSend: function(xhr){xhr.setRequestHeader('Authorization', 'Bearer '+ gapi.auth.getToken().access_token);},
        //  success:    function(result){ console.log("wtf...",result) }
        //});
      }
    });

    // also set the username
    var requestUsername = gapi.client.drive.about.get({fields:"user"});
    requestUsername.execute(function(resp) {
      userstuff = resp.user;
      viewSetUserName(userstuff.displayName);
      console.log(userstuff);
    });
}
